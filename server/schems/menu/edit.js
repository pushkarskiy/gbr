const { textField, linkField, checkboxField, labelField, selectField, numberField } = require('../fields')

const id = labelField('id', 'ID')
const active = checkboxField('active', 'Активен ?')
const title = textField('title', 'Название меню')
const link = linkField('link', 'Ссылка')
const category = selectField('category', 'Категория', ['main-menu', 'about-compony', 'for-client'])
const position = numberField('position', 'Порядок сортировки')
const updatedAt = labelField('updated_at', 'Изменено')
const createdAt = labelField('created_at', 'Создано')

module.exports = [id, updatedAt, createdAt, active, position, title, link, category]
