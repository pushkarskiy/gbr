const { textField, linkField, checkboxField, selectField, numberField } = require('../fields')

const active = checkboxField('active', 'Активен ?')
const title = textField('title', 'Название меню')
const link = linkField('link', 'Ссылка')
const position = numberField('position', 'Порядок сортировки')
const category = selectField('category', 'Категория', ['main-menu', 'about-compony', 'for-client'])

module.exports = [active, title, link, position, category]
