const { checkboxField, labelField, editLinkField } = require('../fields')

const id = editLinkField('id', 'ID')
const active = checkboxField('active', 'Активен ?')
const title = editLinkField('title', 'Название меню')
const link = labelField('link', 'Ссылка')
const category = labelField('category', 'Категория')
const position = labelField('position', 'Порядок сортировки')
const updatedAt = labelField('updated_at', 'Изменено')
const createdAt = labelField('created_at', 'Создано')

module.exports = [id, active, title, link, category, position, updatedAt, createdAt]
