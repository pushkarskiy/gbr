const { textField, textareaField, checkboxField, imageField } = require('../fields')

const active = checkboxField('active', 'Активен ?')
const title = textField('title', 'Название услуги')
const description = textareaField('description', 'Описание')
const image = imageField('image', 'Фото')
const seoTitle = textareaField('seo_title', 'Seo title', '')
const seoDescription = textareaField('seo_description', 'Seo description', '')
const seoKeywords = textareaField('seo_keywords', 'Seo keywords', '')

module.exports = [active, title, seoTitle, seoDescription, seoKeywords, description, image]
