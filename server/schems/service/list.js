const { checkboxField, labelField, editLinkField, imageField, slugLinkField } = require('../fields')

const id = editLinkField('id', 'ID')
const active = checkboxField('active', 'Активен ?')
const title = editLinkField('title', 'Название услуги')
const slug = slugLinkField('slug', 'seo url')
const image = imageField('image', 'Фото')
const updatedAt = labelField('updated_at', 'Изменено')
const createdAt = labelField('created_at', 'Создано')

module.exports = [id, active, title, slug, image, updatedAt, createdAt]
