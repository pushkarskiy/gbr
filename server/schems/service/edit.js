const { textField, textareaField, checkboxField, labelField, imageField } = require('../fields')

const id = labelField('id', 'ID')
const active = checkboxField('active', 'Активен ?')
const title = textField('title', 'Название услуги')
const slug = textField('slug', 'seo url', '', true)
const seoTitle = textareaField('seo_title', 'Seo title', '')
const seoDescription = textareaField('seo_description', 'Seo description', '')
const seoKeywords = textareaField('seo_keywords', 'Seo keywords', '')
const image = imageField('image', 'Фото')
const description = textareaField('description', 'Описание')
const updatedAt = labelField('updated_at', 'Изменено')
const createdAt = labelField('created_at', 'Создано')

module.exports = [id, updatedAt, createdAt, active, title, slug, seoTitle, seoDescription, seoKeywords, description, image]
