const { textField, textareaField, numberField, checkboxField, imageField, labelField } = require('../fields')

const id = labelField('id', 'ID')
const active = checkboxField('active', 'Активен ?')
const title = textField('title', 'Название товара')
const slug = textField('slug', 'seo url', '', true)
const description = textareaField('description', 'Описание продукта')
const price = numberField('price', 'Цена')
const position = numberField('position', 'Порядок сортировки')
const image = imageField('image', 'Фото')
const updatedAt = labelField('updated_at', 'Изменено')
const createdAt = labelField('created_at', 'Создано')

module.exports = [id, updatedAt, createdAt, active, title, slug, description, price, image, position]
