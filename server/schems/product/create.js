const { textField, textareaField, numberField, checkboxField, imageField } = require('../fields')

const active = checkboxField('active', 'Активен ?')
const title = textField('title', 'Название товара')
const description = textareaField('description', 'Описание продукта')
const position = numberField('position', 'Порядок сортировки')
const price = numberField('price', 'Цена')
const image = imageField('image', 'Фото')

module.exports = [active, title, description, price, image, position]
