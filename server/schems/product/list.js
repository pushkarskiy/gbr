const { labelField, checkboxField, imageField, editLinkField } = require('../fields')

const id = editLinkField('id', 'ID')
const active = checkboxField('active', 'Активен ?')
const title = editLinkField('title', 'Название товара')
const slug = editLinkField('slug', 'seo url')
const price = labelField('price', 'Цена')
const position = labelField('position', 'Порядок сортировки')
const image = imageField('image', 'Фото')
const updatedAt = labelField('updated_at', 'Изменено')
const createdAt = labelField('created_at', 'Создано')

module.exports = [id, active, title, slug, image, price, position, updatedAt, createdAt]
