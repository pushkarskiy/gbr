const { labelField, checkboxField, editLinkField, htmlField } = require('../fields')

const id = editLinkField('id', 'ID')
const active = checkboxField('active', 'Активен ?')
const name = editLinkField('name', 'Название меню')
const updatedAt = labelField('updated_at', 'Изменено')
const createdAt = labelField('created_at', 'Создано')

module.exports = [id, active, name, updatedAt, createdAt]
