const { textField, checkboxField } = require('../fields')

const active = checkboxField('active', 'Активен ?')
const name = textField('name', 'Название инфо блока')
const value = textField('value', 'Значение')
const type = textField('type', 'Тип', 'string', true)

module.exports = [ active, name, type, value ]
