const { textField, htmlField, checkboxField } = require('../fields')

const active = checkboxField('active', 'Активен ?')
const name = textField('name', 'Название инфо блока')
const value = htmlField('value', 'Значение')
const type = textField('type', 'Тип', 'html', true)

module.exports = [ active, name, type, value ]
