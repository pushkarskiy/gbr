const { textField, checkboxField, labelField, selectField } = require('../fields')

const id = labelField('id', 'ID')
const active = checkboxField('active', 'Активен ?')
const name = textField('name', 'Название меню')
const value = textField('value', 'Значение')
const type = selectField('type', 'Тип', [ 'string', 'text', 'html' ], true)
const updatedAt = labelField('updated_at', 'Изменено')
const createdAt = labelField('created_at', 'Создано')

module.exports = [ id, updatedAt, createdAt, active, name, type, value ]
