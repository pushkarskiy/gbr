const { textField, checkboxField, labelField, selectField, fileField } = require('../fields')

const id = labelField('id', 'ID')
const active = checkboxField('active', 'Активен ?')
const name = textField('name', 'Название меню')
const value = fileField('value', 'File')
const type = selectField('type', 'Тип', [ 'string', 'text', 'html', 'file' ], true)
const updatedAt = labelField('updated_at', 'Изменено')
const createdAt = labelField('created_at', 'Создано')

module.exports = [ id, updatedAt, createdAt, active, name, type, value ]
