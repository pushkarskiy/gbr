const { textField, checkboxField, fileField } = require('../fields')

const active = checkboxField('active', 'Активен ?')
const name = textField('name', 'Название инфо блока')
const value = fileField('value', 'File')
const type = textField('type', 'Тип', 'file', true)

module.exports = [ active, name, type, value ]
