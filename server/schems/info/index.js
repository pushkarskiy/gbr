const createString = require('./createString')
const createText = require('./createText')
const createHtml = require('./createHtml')
const createFile = require('./createFile')
const createImage = require('./createImage')

const editString = require('./editString')
const editText = require('./editText')
const editHtml = require('./editHtml')
const editFile = require('./editFile')
const editImage = require('./editImage')

const listString = require('./listString')
const listText = require('./listText')
const listHtml = require('./listHtml')
const listFile = require('./listFile')
const listImage = require('./listImage')

module.exports = {
  createString,
  createFile,
  createImage,
  createText,
  createHtml,
  editString,
  editFile,
  editImage,
  editText,
  editHtml,
  listString,
  listFile,
  listImage,
  listText,
  listHtml,
}
