
const { textField, checkboxField, imageField } = require('../fields')

const active = checkboxField('active', 'Активен ?')
const name = textField('name', 'Название инфо блока')
const value = imageField('value', 'File')
const type = textField('type', 'Тип', 'file', true)

module.exports = [ active, name, type, value ]
