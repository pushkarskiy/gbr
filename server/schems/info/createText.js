const { textField, textareaField, checkboxField } = require('../fields')

const active = checkboxField('active', 'Активен ?')
const name = textField('name', 'Название инфо блока')
const value = textareaField('value', 'Значение')
const type = textField('type', 'Тип')

module.exports = [ active, name, type, value ]
