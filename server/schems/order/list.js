const { labelField, orderTypeField } = require('../fields')

const id = labelField('id', 'ID')
const name = labelField('name', 'ФИО')
const phone = labelField('phone', 'Телефон')
const email = labelField('email', 'E-mail')
const orderType = orderTypeField('order_type', 'Order type')
const createdAt = labelField('created_at', 'Создано') 

module.exports = [ id, name, phone, email, orderType, createdAt ]
