const textField = (name = 'name', label = 'label', defaultValue = '', disabled = false) => ({
  type: 'text',
  name,
  label,
  defaultValue,
  disabled
})

const textareaField = (name = 'name', label = 'label', defaultValue = '', disabled = false) => ({
  type: 'textarea',
  name,
  label,
  defaultValue,
  disabled
})

const numberField = (name = 'name', label = 'label', defaultValue = 0, disabled = false) => ({
  type: 'text',
  name,
  label,
  defaultValue,
  disabled
})

const imageField = (name = 'name', label = 'label', disabled = false) => ({
  type: 'image',
  label,
  name,
  disabled
})

const fileField = (name = 'name', label = 'label') => ({
  type: 'file',
  label,
  name
})

const sizeField = (name = 'name', label = 'label') => ({
  type: 'sizeField',
  label,
  name
})

const checkboxField = (name = 'name', label = 'label', defaultValue = false, disabled = false) => ({
  type: 'checkbox',
  label,
  name,
  defaultValue,
  disabled
})

const textHtmlField = (name = 'name', label = 'label', defaultValue = '', disabled = false) => ({
  type: 'textHtml',
  label,
  name,
  defaultValue,
  disabled
})

const htmlField = (name = 'name', label = 'label', defaultValue = '', disabled = false) => ({
  type: 'html',
  label,
  name,
  defaultValue,
  disabled
})

const labelField = (name = 'name', label = 'label', disabled = false) => ({
  type: 'labelField',
  label,
  name,
  disabled
})

const selectField = (name = 'name', label = 'label', options = [], disabled = false) => ({
  type: 'select',
  label,
  name,
  options,
  disabled
})

const idField = (name = 'name', label = 'label', disabled = false) => ({
  type: 'id',
  label,
  name,
  disabled
})

const linkField = (name = 'name', label = 'label', disabled = false) => ({
  type: 'link',
  label,
  name,
  disabled
})

const editLinkField = (name = 'name', label = 'label', disabled = false) => ({
  type: 'editLink',
  label,
  name,
  disabled
})

const slugLinkField = (name = 'name', label = 'label', disabled = false) => ({
  type: 'slugLinkField',
  label,
  name,
  disabled
})

const fileLinkField = (name = 'name', label = 'label', disabled = false) => ({
  type: 'fileLinkField',
  label,
  name,
  disabled
})

const fileTypeField = (name = 'name', label = 'label', disabled = false) => ({
  type: 'fileTypeField',
  label,
  name,
  disabled
})

const orderTypeField = (name = 'name', label = 'label') => ({
  type: 'orderTypeField',
  label,
  name
})

module.exports = {
  textField,
  textareaField,
  numberField,
  checkboxField,
  imageField,
  fileField,
  textHtmlField,
  htmlField,
  idField,
  linkField,
  editLinkField,
  selectField,
  labelField,
  sizeField,
  slugLinkField,
  fileLinkField,
  fileTypeField,
  orderTypeField
}
