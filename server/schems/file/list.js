const { labelField, sizeField } = require('../fields')

const path = labelField('path', 'Путь к файлу')
const clientName = labelField('client_name', 'Название')
const size = sizeField('size', 'Размер')
const extname = labelField('extname', 'Тип')
const updatedAt = labelField('updated_at', 'Изменено')
const createdAt = labelField('created_at', 'Создано')

module.exports = [ clientName, path, size, extname, updatedAt, createdAt ] 
