const { textField, textHtmlField, checkboxField, labelField, textareaField } = require('../fields')

const id = labelField('id', 'ID')
const active = checkboxField('active', 'Активен ?')
const title = textField('title', 'Название')
const slug = textField('slug', 'Seo url', '', true)
const seoTitle = textareaField('seo_title', 'Seo title', '')
const seoDescription = textareaField('seo_description', 'Seo description', '')
const seoKeywords = textareaField('seo_keywords', 'Seo keywords', '')
const content = textHtmlField('content', 'Текст')
const updatedAt = labelField('updated_at', 'Изменено')
const createdAt = labelField('created_at', 'Создано')

module.exports = [id, updatedAt, createdAt, active, slug, title, seoTitle, seoDescription, seoKeywords, content ]
