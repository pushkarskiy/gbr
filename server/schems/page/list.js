const { labelField, checkboxField, editLinkField, slugLinkField } = require('../fields')

const id = editLinkField('id', 'ID')
const active = checkboxField('active', 'Активен ?')
const title = editLinkField('title', 'Название')
const slug = slugLinkField('slug', 'seo url')
const updatedAt = labelField('updated_at', 'Изменено')
const createdAt = labelField('created_at', 'Создано')

module.exports = [ id, active, title, slug, updatedAt, createdAt ]
