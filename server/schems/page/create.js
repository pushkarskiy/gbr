const { textField, textHtmlField, checkboxField, textareaField } = require('../fields')

const active = checkboxField('active', 'Активен ?')
const title = textField('title', 'Название')
const content = textHtmlField('content', 'Текст')
const seoTitle = textareaField('seo_title', 'Seo title', '')
const seoDescription = textareaField('seo_description', 'Seo description', '')
const seoKeywords = textareaField('seo_keywords', 'Seo keywords', '')

module.exports = [active, title, seoTitle, seoDescription, seoKeywords, content]
