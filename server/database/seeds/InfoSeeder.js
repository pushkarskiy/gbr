'use strict'

/*
|--------------------------------------------------------------------------
| InfoSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Database')} */

const Database = use('Database')

class InfoSeeder {
  async run() {
    const infosMock = [
      {
        created_at: Date.now(),
        updated_at: Date.now(),
        name: 'phone',
        value: '8 302 453 45 45',
        type: 'string',
        active: 1
      },
      {
        created_at: Date.now(),
        updated_at: Date.now(),
        name: 'mobile-phone',
        value: '8 302 453 45 45',
        type: 'string',
        active: 1
      },
      {
        created_at: Date.now(),
        updated_at: Date.now(),
        name: 'email',
        value: 'info@br75.ru',
        type: 'string',
        active: 1
      },
      {
        created_at: Date.now(),
        updated_at: Date.now(),
        name: 'address',
        value: '674673, г. Краснокаменск, ул.Молодежная 26Г, пом. 4',
        type: 'string',
        active: 1
      },
      {
        created_at: Date.now(),
        updated_at: Date.now(),
        name: 'promo_service',
        value: '1',
        type: 'string',
        active: 1
      },
      {
        created_at: Date.now(),
        updated_at: Date.now(),
        name: 'promo_image',
        value: '/images/main-box-image.jpg',
        type: 'image',
        active: 1
      },
      {
        created_at: Date.now(),
        updated_at: Date.now(),
        name: 'promo_title',
        value: `
          ПЕРВЫЕ 300 КЛИЕНТОВ
          ПЕРЕХОД БЕСПЛАТНО
        `,
        type: 'text',
        active: 1
      },
      {
        created_at: Date.now(),
        updated_at: Date.now(),
        name: 'analitics',
        value: `<!-- This need to pass metrict Yandex, Google -->`,
        type: 'html',
        active: 1
      },
      {
        created_at: Date.now(),
        updated_at: Date.now(),
        name: 'contacts',
        value: `
            <div class="contacts-column contacts-info">
              <div class="contact-block">
                  <div class="contact-block-title">
                      Общество с ограниченой ответственностью частная охранная организация «глобал безопасность регион»
                  </div>
                  <div class="contact-block-info">
                      <b>Юридический адрес:</b> 672014, РФ, Забайкальский край, гор. Чита, ул. Тракт Романовский км. 9, офис 2
                  </div>
                  <div class="contact-block-info">
                      <b>Почтовый адрес:</b> 674673, г. Краснокаменск, ул. Молодежная 25Г, пом. 4
                  </div>
                  <div class="contact-block-info">
                      <b>ИНН:</b> 7536173992
                  </div>
                  <div class="contact-block-info">
                      <b>КПП:</b> 753601001
                  </div>
                  <div class="contact-block-info">
                      <b>Банковские реквизиты:</b> Отделение № 8600 ПАО «Сбербанк России» г. Чита БИК 047601637 Кор./сч. 3010181050000000637 Расчетный счет % 40702810074000005046 Лицензия на осуществление охранной деятельности № 325, выдана Управлением Росгвардии по Забайкальскому краю.
                  </div>
              </div>
          </div>

          <div class="contacts-column contacts-detail">
              <div class="contact-block">
                  <h3 class="contact-block-caption">Офис в г. Краснокаменск</h3>
                  <div class="contact-block-box">
                      <div class="contact-block-item">
                          <i class="fas fa-mobile"></i> Тел:&nbsp;8 302 453 45 45
                      </div>
                      <div class="contact-block-item">
                          <i class="fas fa-phone"></i> Сот:&nbsp;8 302 453 45 45</div>
                      <div class="contact-block-item">
                          <i class="fas fa-envelope"></i> E-mail:&nbsp;info@br75.ru
                      </div>
                      <div class="contact-block-item">
                          <i class="fas fa-map-marker-alt"></i> Адрес:&nbsp;674673, г. Краснокаменск,
                          <br />ул. Молодежная 26Г, пом. 4
                      </div>
                      <div class="contact-block-item">
                          <i class="fas fa-user"></i> Менеджер по работе с клиентами Тарасов Леонид Геннадьевич
                      </div>
                  </div>
              </div>

              <div class="contact-block">
                  <h3 class="contact-block-caption">Техническая потдержка</h3>
                  <div class="contact-block-box">
                      <div class="contact-block-item">
                          <i class="fas fa-mobile"></i> Тел:&nbsp;8 302 453 45 45
                      </div>
                      <div class="contact-block-item">
                          <i class="fas fa-phone"></i> Сот:&nbsp;8 302 453 45 45</div>
                      <div class="contact-block-item">
                          <i class="fas fa-envelope"></i> E-mail:&nbsp;info@br75.ru
                      </div>
                      <div class="contact-block-item">
                          <i class="fas fa-map-marker-alt"></i> Адрес:&nbsp;674673, г. Краснокаменск,
                          <br />ул. Молодежная 26Г, пом. 4
                      </div>
                      <div class="contact-block-item">
                          <i class="fas fa-user"></i> Инженер Тиранов Николай Николаевич
                      </div>
                  </div>
              </div>

              <div class="contact-block">
                  <h3 class="contact-block-caption">Отдел кадров</h3>
                  <div class="contact-block-box">
                      <div class="contact-block-item">
                          <i class="fas fa-mobile"></i> Тел:&nbsp;8 302 453 45 45
                      </div>
                      <div class="contact-block-item">
                          <i class="fas fa-phone"></i> Сот:&nbsp;8 302 453 45 45</div>
                      <div class="contact-block-item">
                          <i class="fas fa-envelope"></i> E-mail:&nbsp;info@br75.ru
                      </div>
                      <div class="contact-block-item">
                          <i class="fas fa-map-marker-alt"></i> Адрес:&nbsp;674673, г. Краснокаменск,
                          <br />ул. Молодежная 26Г, пом. 4
                      </div>
                      <div class="contact-block-item">
                          <i class="fas fa-user"></i> Директор Синяк Павел Михайлович
                      </div>
                  </div>
              </div>
          </div>
        `,
        type: 'html',
        active: 1
      },
      {
        created_at: Date.now(),
        updated_at: Date.now(),
        name: 'promo_description',
        value: `
          <p>При заключении договора на пультовую или физическую охрану в срок до 31.12.2019 года, оборудование для охранной сигнализации Вы получаете Подарок!</p>
          <p>Акция действует для организзаций и предпринимателей.</p>`,
        type: 'html',
        active: 1
      }
    ]

    await Database.from('infos').insert(infosMock)
  }
}

module.exports = InfoSeeder
