'use strict'

/*
|--------------------------------------------------------------------------
| MenuSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Database')} */

const Database = use('Database')

class MenuSeeder {
  async run() {
    const menusMock = [
      {
        created_at: Date.now(),
        updated_at: Date.now(),
        title: 'Техническая поддержка',
        link: '/support',
        category: 'main-menu',
        active: 1,
      },
      {
        created_at: Date.now(),
        updated_at: Date.now(),
        title: 'Наши услуги',
        link: '/services',
        category: 'main-menu',
        active: 1,
      },
      {
        created_at: Date.now(),
        updated_at: Date.now(),
        title: 'Общая информация',
        link: '/pages/1',
        category: 'about-company',
        active: 1,
      },
      {
        created_at: Date.now(),
        updated_at: Date.now(),
        title: 'Реквизиты',
        link: '/contacts',
        category: 'about-company',
        active: 1,
      },
      {
        created_at: Date.now(),
        updated_at: Date.now(),
        title: 'Техническая поддержка',
        link: '/support',
        category: 'for-client',
        active: 1,
      },
    ]

    await Database.from('menus').insert(menusMock)
  }
}

module.exports = MenuSeeder
