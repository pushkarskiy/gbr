'use strict'

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */

const Factory = use('Factory')
const slugify = require('@sindresorhus/slugify')
const { name, commerce, lorem, internet, phone, random } = require('faker/locale/ru')

const product = () => ({
  title: name.title(),
  image: '/images/p1.png',
  description: lorem.text(),
  price: commerce.price(),
  active: true
})

const service = () => ({
  title: name.title(),
  image: '/images/p2.png',
  'preview-image': '/images/b.jpg',
  description: lorem.text(),
  active: true
})

const page = () => ({
  title: name.title(),
  content: lorem.text(),
  active: true
})

const withSlug = (data = {}) => ({
  ...data,
  slug: slugify(data.title)
})

const order = (data={}) => ({
  name: name.findName(),
  phone: phone.phoneNumber(),
  email: internet.email(),
  order_type: random.arrayElement([ 'callme', 'service', 'product' ]),
  type_id: 0
})

Factory.blueprint('App/Models/Product', async () => {
  const d = product()
  return withSlug(d)
})

Factory.blueprint('App/Models/Service', async () => {
  const d = service()
  return withSlug(d)
})

Factory.blueprint('App/Models/Page', async () => {
  const d = page()
  return withSlug(d)
})

Factory.blueprint('App/Models/Order', async () => {
  return order()
})