'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MenusSchema extends Schema {
  up() {
    this.create('menus', (table) => {
      table.increments()
      table.timestamps()
      table.string('title', 100).notNullable()
      table.string('link', 100).notNullable()
      table.integer('position').default(0)
      table.enu('category', ['main-menu', 'about-company', 'for-client'])
      table.boolean('active').notNullable().default(false)
    })
  }

  down() {
    this.drop('menus')
  }
}

module.exports = MenusSchema
