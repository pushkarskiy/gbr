'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrdersSchema extends Schema {
  up() {
    this.create('orders', (table) => {
      table.increments()
      table.timestamps()
      table.string('name', 100).notNullable()
      table.string('phone').notNullable()
      table.string('email').notNullable()
      table.enu('order_type', [ 'callme', 'service', 'product' ])
      table.integer('type_id').default(0)
    })
  }

  down() {
    this.drop('orders')
  }
}

module.exports = OrdersSchema
