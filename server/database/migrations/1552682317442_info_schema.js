'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class InfoSchema extends Schema {
  up() {
    this.create('infos', (table) => {
      table.increments()
      table.timestamps()
      table.string('name', 100).notNullable().unique()
      table.text('value').notNullable()
      table.enu('type', [ 'string', 'text', 'html', 'image', 'file' ])
      table.boolean('active').notNullable().default(false)
    })
  }

  down() {
    this.drop('infos')
  }
}

module.exports = InfoSchema
