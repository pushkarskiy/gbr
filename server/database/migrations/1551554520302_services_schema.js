'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ServicesSchema extends Schema {
  up() {
    this.create('services', (table) => {
      table.increments()
      table.timestamps()
      table.string('title', 100).notNullable()
      table.string('slug', 200).notNullable().unique()
      table.string('seo_title', 200)
      table.string('seo_description', 200)
      table.string('seo_keywords', 200)
      table.string('image').notNullable()
      table.string('preview-image')
      table.text('description')
      table.boolean('active').notNullable().default(false)
    })
  }

  down() {
    this.drop('services')
  }
}

module.exports = ServicesSchema
