'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PagesSchema extends Schema {
  up() {
    this.create('pages', (table) => {
      table.increments()
      table.timestamps()
      table.string('title', 100).notNullable()
      table.string('slug', 200).notNullable().unique()
      table.string('seo_title', 200)
      table.string('seo_description', 200)
      table.string('seo_keywords', 200)
      table.text('content')
      table.boolean('active').notNullable().default(false)
    })
  }

  down() {
    this.drop('pages')
  }
}

module.exports = PagesSchema
