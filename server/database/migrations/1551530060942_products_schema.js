'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductsSchema extends Schema {
  up() {
    this.create('products', (table) => {
      table.increments()
      table.timestamps()
      table.string('title', 100).notNullable()
      table.string('slug', 200).notNullable().unique()
      table.string('image').notNullable()
      table.text('description')
      table.integer('price').default(0)
      table.integer('position').default(0)
      table.boolean('active').notNullable().default(false)
    })
  }

  down() {
    this.drop('products')
  }
}

module.exports = ProductsSchema
