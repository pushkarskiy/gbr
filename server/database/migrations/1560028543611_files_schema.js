'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FilesSchema extends Schema {
  up () {
    this.create('files', (table) => {
      table.increments()
      table.timestamps()
      table.string('client_name').notNullable()
      table.string('path').notNullable()
      table.integer('size').default(0)
      table.string('extname').notNullable()
    })
  }

  down () {
    this.drop('files')
  }
}

module.exports = FilesSchema
