'use strict';

(function(window, document) {
	function ready() {
		const SHOW_CLASS = 'show';

		const OVERLAY_SELECTOR = '.overlay';
		const MODAL_CLOSE_SELECTOR = '.modal-close';
		const MODAL_FORM_SELECTOR = '.modal';
		const MODAL_ERROR_MESSAGE_SELECTOR = '.modal-message-error';
		const MODAL_SUCCESS_MESSAGE_SELECTOR = '.modal-message-success';
		const ORDER_ID_ATTR = 'data-order-id';
		const ORDER_TYPE_ATTR = 'data-order-type';
		const ORDER_ID_SELECTOR = '[' + ORDER_ID_ATTR + ']';

		const FORM_ACTION_URL = '/order';

		const triggers = document.querySelectorAll(ORDER_ID_SELECTOR);
		const close = document.querySelector(MODAL_CLOSE_SELECTOR);
		const form = document.querySelector(MODAL_FORM_SELECTOR);

		let orderId = null;
		let orderType = null;

		triggers.forEach(function(trigger) {
			trigger.addEventListener('click', showModal.bind(this, trigger));
		});

		close.addEventListener('click', hideModal);
		form.addEventListener('submit', submitForm);

		function showModal(trigger) {
			const overlay = document.querySelector(OVERLAY_SELECTOR);

			orderId = trigger.getAttribute(ORDER_ID_ATTR);
			orderType = trigger.getAttribute(ORDER_TYPE_ATTR);

			overlay.classList.add(SHOW_CLASS);
		}

		function hideModal() {
			const overlay = document.querySelector(OVERLAY_SELECTOR);

			form.reset();
			clearMessages();

			orderId = null;
			orderType = null;

			overlay.classList.remove(SHOW_CLASS);
		}

		function showError() {
			const error = document.querySelector(MODAL_ERROR_MESSAGE_SELECTOR);

			error.classList.add(SHOW_CLASS);
		}

		function showSuccess() {
			const success = document.querySelector(MODAL_SUCCESS_MESSAGE_SELECTOR);

			success.classList.add(SHOW_CLASS);
		}

		function clearMessages() {
			const error = document.querySelector(MODAL_ERROR_MESSAGE_SELECTOR);
			const success = document.querySelector(MODAL_SUCCESS_MESSAGE_SELECTOR);

			success.classList.remove(SHOW_CLASS);
			error.classList.remove(SHOW_CLASS);
		}

		function submitForm(event) {
			event.preventDefault();
			const inputs = event.target.querySelectorAll('input[name]');
			const formData = new FormData();

			clearMessages();

			for (let i = 0; i < inputs.length; i++) {
				let name = inputs[i].name;
				let value = inputs[i].value;
				formData.append(name, value);
			}

			formData.append('order_type', orderType);
			formData.append('type_id', orderId);

			sendData(formData, showSuccess, showError);
		}

		function sendData(data, showSuccess, showError) {
			const XHR = new XMLHttpRequest();

			XHR.open('POST', FORM_ACTION_URL);
			XHR.timeout = 3000;
			XHR.onreadystatechange = function() {
				if (XHR.readyState !== 4) return;

				if (XHR.status != 200) {
					showError();
					return;
				}

				try {
					const resp = JSON.parse(XHR.responseText);
					resp.status === 'ok' ? showSuccess() : showError();
				} catch (e) {
					showError();
					console.log(e);
				}
			};

			XHR.send(data);
		}
	}
	window.addEventListener('DOMContentLoaded', ready);
})(window, document);
