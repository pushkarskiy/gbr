const Info = use('App/Models/Info')

class InfoService {
  async get() {
    const infos = await Info.query().where('active', 1).fetch()

    return infos.rows.reduce(this.format, {})
  }

  format(prevValue, { name, value, type }) {
    prevValue[name] = { value, type }
    return prevValue
  }
}

module.exports = new InfoService()
