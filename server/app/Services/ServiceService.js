const Service = use('App/Models/Service')

class ServiceService {
  async check(slug, limit = 1) {
    const services = await Service.query().where('active', 1).where('slug', slug).limit(limit).fetch()

    return Boolean(services.rows.length)
  }

  async get(slug) {
    const services = await Service.query().where('active', 1).where('slug', slug).limit(1).fetch()

    return services.rows.map(this.format)[0]
  }

  async getAll() {
    const services = await Service.query().where('active', 1).fetch()

    return services.rows.map(this.formatServices)
  }

  async getContent(slug) {
    const services = await Service.query().where('active', 1).where('slug', slug).limit(1).fetch()

    return services.rows.map(this.formatContent)[0]
  }

  async getPreview(slug) {
    const services = await Service.query().where('active', 1).where('slug', slug).limit(1).fetch()

    return services.rows[0]
  }

  async getServicesPreview(limit = 4) {
    const services = await Service.query().where('active', 1).limit(limit).fetch()

    return services.rows.map(this.formatServices)
  }

  async getSubMenu(limit = 4) {
    const menu = await Service.query().where('active', 1).limit(limit).fetch()

    return menu.rows.map(this.formatSubMenu)
  }

  async getBreadcrumbLastItem(slug, limit = 1) {
    const services = await Service.query().where('active', 1).where('slug', slug).limit(limit).fetch()

    return services.rows.map(this.formatSubMenu)
  }

  formatSubMenu({ title, slug }) {
    return { title, link: `/services/${slug}` }
  }

  format({ title, image, slug, description }) {
    return { title, image, description, slug }
  }

  formatContent({ id, title, image, slug, description, seo_title, seo_description, seo_keywords }) {
    return { id, title, image, content: description, slug, seo_title, seo_description, seo_keywords }
  }

  formatServices({ title, image, slug, description }) {
    return { title, image, description, link: `/services/${slug}` }
  }
}

module.exports = new ServiceService()
