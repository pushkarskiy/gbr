const Menu = use('App/Models/Menu')

class MenuService {
  async get(category, limit = 3) {
    const menus = limit
      ? await Menu.query().where('active', 1).where('category', category).orderBy('position', 'desc').limit(limit).fetch()
      : await Menu.query().where('active', 1).where('category', category).orderBy('position', 'desc').fetch()

    return menus.rows.map(this.format)
  }

  format({ title, link }) {
    return { title, link }
  }
}

module.exports = new MenuService()
