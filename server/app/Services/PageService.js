const Page = use('App/Models/Page')

class PageService {
  async check(slug, limit = 1) {
    const pages = await Page.query().where('active', 1).where('slug', slug).limit(limit).fetch()

    return Boolean(pages.rows.length)
  }

  async getPageBySlug(slug, limit = 1) {
    const pages = await Page.query().where('active', 1).where('slug', slug).limit(limit).fetch()

    return pages.rows.map(this.format)[0]
  }

  async getBreadcrumbLastItem(slug, limit = 1) {
    const pages = await Page.query().where('active', 1).where('slug', slug).limit(limit).fetch()

    return pages.rows.map(this.formatBreadcrumb)
  }

  format({ title, content, seo_title, seo_description, seo_keywords }) {
    return { title, content, seo_title, seo_description, seo_keywords }
  }

  formatBreadcrumb({ title, id }) {
    return { title, link: `/pages/${id}` }
  }
}

module.exports = new PageService()
