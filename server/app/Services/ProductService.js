const Product = use('App/Models/Product')

class ProductService {
  async check(slug, limit = 1) {
    const products = await Product.query().where('active', 1).where('slug', slug).limit(limit).fetch()

    return Boolean(products.rows.length)
  }

  
  async getAll() {
    const products = await Product.query().where('active', 1).fetch()

    return products.rows.map(this.formatProducts)
  }

  async getContent(slug) {
    const products = await Product.query().where('active', 1).where('slug', slug).limit(1).fetch()

    return products.rows.map(this.formatContent)[0]
  }

  async getBreadcrumbLastItem(slug, limit = 1) {
    const products = await Product.query().where('active', 1).where('slug', slug).limit(limit).fetch()

    return products.rows.map(this.formatSubMenu)
  }

  async getPreview(limit = 3) {
    const products = await Product.query().where('active', 1).orderBy('position', 'desc').limit(limit).fetch()

    return products.rows.map(this.format)
  }

  format({ title, image, price, slug }) {
    return { title, image, price, link: `/products/${slug}` }
  }

  formatSubMenu({ title, slug }) {
    return { title, link: `/products/${slug}` }
  }

  formatContent({ id, title, image, slug, description, seo_title, seo_description, seo_keywords }) {
    return { id, title, image, content: description, slug, seo_title, seo_description, seo_keywords }
  }

  formatProducts({ title, image, slug, description }) {
    return { title, image, description, link: `/products/${slug}` }
  }
}

module.exports = new ProductService()
