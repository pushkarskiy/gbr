'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const slugify = require('@sindresorhus/slugify')
const Service = use('App/Models/Service')
const { validate } = use('Validator')

const ERROR_MASSAGE_ADD_SERVICE = 'Ошибка добавления услуги'
const ERROR_MASSAGE_SERVICE_IS_NOT_EXIST = 'Такой услуги не существует'
const ERROR_MASSAGE_SERVICE_IS_EXIST = 'Нет изменений для сохранения'

class ServiceController {
  /**
   * Show a list of all services.
   * GET services
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    const page = request.input('page')
    const limit = request.input('limit')
    const services = page ? await Service.query().paginate(page, limit) : await Service.all()

    return response.json({ status: 'ok', data: services })
  }

  /**
   * Create/save a new service.
   * POST temps
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    const rules = {
      title: 'required|min:3|max:1000',
      seo_title: 'min:3|max:200',
      seo_description: 'min:3|max:500',
      seo_keywords: 'min:3|max:500',
      image: 'min:3',
    }

    const input = request.only(['title', 'seo_title', 'seo_description', 'seo_keywords', 'image'])
    const validation = await validate(input, rules)

    if (validation.fails()) {
      const validationError = {
        status: 'validationError',
        message: validation.messages(),
      }

      return response.json(validationError)
    }

    const data = { ...input, slug: slugify(input.title) }
    const service = await Service.create(data)

    if (!service.id) {
      response.json({ status: 'error', message: ERROR_MASSAGE_ADD_SERVICE })
    }

    response.json({ status: 'ok', data: service.id })
  }

  /**
   * Display a single service.
   * GET services/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
    const { id } = params
    const service = await Service.find(id)

    if (!service) {
      const serviceIsNotExist = {
        status: 'error',
        message: ERROR_MASSAGE_SERVICE_IS_NOT_EXIST,
      }
      return response.json(serviceIsNotExist)
    }

    return response.json({ status: 'ok', data: service })
  }

  /**
   * Update service details.
   * PUT or PATCH services/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const { id } = params
    const input = request.only(['title', 'description', 'active', 'seo_title', 'seo_description', 'seo_keywords', 'image'])
    const rules = {
      title: 'min:3|max:1000',
      description: 'min:3|max:10000',
      active: 'boolean',
      seo_title: 'min:3|max:200',
      seo_description: 'min:3|max:500',
      seo_keywords: 'min:3|max:500',
      image: 'min:3'
    }

    const validation = await validate(input, rules)

    if (validation.fails()) {
      const validationError = {
        status: 'validationError',
        message: validation.messages(),
      }

      return response.json(validationError)
    }

    const service = await Service.find(id)

    Object.assign(service, input)

    const serviceId = await service.save()

    if (!serviceId) {
      return response.json({
        status: 'error',
        message: ERROR_MASSAGE_SERVICE_IS_EXIST,
      })
    }

    return response.json({ status: 'ok', data: serviceId })
  }

  /**
   * Delete a service with id.
   * DELETE services/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    const { id } = params
    const service = await Service.find(id)

    if (!service) {
      const serviceIsNotExist = {
        status: 'error',
        message: ERROR_MASSAGE_SERVICE_IS_NOT_EXIST,
      }
      return response.json(serviceIsNotExist)
    }

    await service.delete()
    return response.json({ status: 'ok', data: null })
  }
}

module.exports = ServiceController
