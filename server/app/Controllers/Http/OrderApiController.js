'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Order = use('App/Models/Order')

const ERROR_MASSAGE_ORDER_IS_NOT_EXIST = 'Такой заказ не существует'

class OrderApiController {
  /**
   * Show a list of all orderapicontrolles.
   * GET orderapicontrolles
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const page = request.input('page')
    const limit = request.input('limit')
    const orders = page ? await Order.query().paginate(page, limit) : await Order.all() 

    return response.json({ status: 'ok', data: orders })
  }

  /**
   * Delete a orderapicontrolle with id.
   * DELETE orderapicontrolles/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    const { id } = params
    const order = await Order.find(id)

    if (!order) {
      const orderIsNotExist = {
        status: 'error',
        message: ERROR_MASSAGE_ORDER_IS_NOT_EXIST,
      }
      return response.json(orderIsNotExist)
    }

    await order.delete()
    
    return response.json({ status: 'ok', data: null })
  }
}

module.exports = OrderApiController
