'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const slugify = require('@sindresorhus/slugify')
const Product = use('App/Models/Product')
const { validate } = use('Validator')

const ERROR_MASSAGE_ADD_PRODUCT = 'Ошибка добавления товара'
const ERROR_MASSAGE_PRODUCT_IS_NOT_EXIST = 'Такой продукт не существует'
const ERROR_MASSAGE_PRODUCT_IS_EXIST = 'Нет изменений для сохранения'

class ProductController {
  /**
   * Show a list of all products.
   * GET products
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view, params }) {
    const page = request.input('page')
    const limit = request.input('limit')
    const products = page ? await Product.query().paginate(page, limit) : await Product.query().orderBy('position', 'desc').fetch()

    return response.json({ status: 'ok', data: products })
  }

  /**
   * Create/save a new product.
   * POST temps
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    const rules = {
      title: 'required|min:3|max:100',
      description: 'required',
      image: 'min:3',
      price: 'required|integer',
    }

    const input = request.only(['title', 'description', 'image', 'price', 'position'])
    const validation = await validate(input, rules)

    if (validation.fails()) {
      const validationError = {
        status: 'validationError',
        message: validation.messages(),
      }

      return response.json(validationError)
    }

    const data = { ...input, slug: slugify(input.title) }
    const product = await Product.create(data)

    if (!product.id) {
      response.json({ status: 'error', message: ERROR_MASSAGE_ADD_PRODUCT })
    }

    response.json({ status: 'ok', data: product.id })
  }

  /**
   * Display a single product.
   * GET products/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
    const { id } = params
    const product = await Product.find(id)

    if (!product) {
      const productIsNotExist = {
        status: 'error',
        message: ERROR_MASSAGE_PRODUCT_IS_NOT_EXIST,
      }
      return response.json(productIsNotExist)
    }

    return response.json({ status: 'ok', data: product })
  }

  /**
   * Update product details.
   * PUT or PATCH products/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const { id } = params
    const input = request.only(['title', 'description', 'image', 'price', 'active', 'position'])
    const rules = {
      title: 'min:3|max:300',
      description: 'min:3|max:1000',
      image: 'min:3',
      price: 'integer',
      active: 'boolean',
    }

    const validation = await validate(input, rules)

    if (validation.fails()) {
      const validationError = {
        status: 'validationError',
        message: validation.messages(),
      }

      return response.json(validationError)
    }

    const product = await Product.find(id)

    Object.assign(product, input)

    const productId = await product.save()

    if (!productId) {
      return response.json({
        status: 'error',
        message: ERROR_MASSAGE_PRODUCT_IS_EXIST,
      })
    }

    return response.json({ status: 'ok', data: productId })
  }

  /**
   * Delete a product with id.
   * DELETE products/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    const { id } = params
    const product = await Product.find(id)

    if (!product) {
      const productIsNotExist = {
        status: 'error',
        message: ERROR_MASSAGE_PRODUCT_IS_NOT_EXIST,
      }
      return response.json(productIsNotExist)
    }

    await product.delete()
    return response.json({ status: 'ok', data: null })
  }
}

module.exports = ProductController
