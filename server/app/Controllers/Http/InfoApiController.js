'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Info = use('App/Models/Info')
const { validate } = use('Validator')

const ERROR_MASSAGE_ADD_INFO = 'Ошибка добавления инфоблока'
const ERROR_MASSAGE_INFO_IS_NOT_EXIST = 'Такой инфоблок не существует'
const ERROR_MASSAGE_INFO_IS_EXIST = 'Нет изменений для сохранения'

class InfoController {
  /**
   * Show a list of all infos.
   * GET infos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view, params }) {
    const { type } = params
    const page = request.input('page')
    const limit = request.input('limit')
    const infos = page ? await Info.query().paginate(page, limit) : await Info.query().where('type', '=', type).fetch()

    return response.json({ status: 'ok', data: infos })
  }

  /**
   * Create/save a new Info.
   * POST temps
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ params, request, response }) {
    const rules = {
      name: 'required|min:3|max:100',
      value: 'required|min:3',
      active: 'boolean',
    }

    const fields = request.only([ 'name', 'value', 'active' ])
    const input = { ...fields, type: params.type}
    const validation = await validate(input, rules)

    if (validation.fails()) {
      const validationError = {
        status: 'validationError',
        message: validation.messages(),
      }

      return response.json(validationError)
    }

    const info = await Info.create(input)

    if (!info.id) {
      response.json({ status: 'error', message: ERROR_MASSAGE_ADD_INFO })
    }

    response.json({ status: 'ok', data: info.id })
  }

  /**
   * Display a single Info.
   * GET infos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
    const { id } = params
    const info = await Info.find(id)

    if (!info) {
      const productIsNotExist = {
        status: 'error',
        message: ERROR_MASSAGE_INFO_IS_NOT_EXIST,
      }
      return response.json(productIsNotExist)
    }

    return response.json({ status: 'ok', data: info })
  }

  /**
   * Update Info details.
   * PUT or PATCH infos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const { id } = params
    const input = request.only([ 'name', 'type', 'value', 'active' ])
    const rules = {
      name: 'required|min:3|max:100',
      type: 'required',
      value: 'required',
      active: 'required',
    }

    const validation = await validate(input, rules)

    if (validation.fails()) {
      const validationError = {
        status: 'validationError',
        message: validation.messages(),
      }

      return response.json(validationError)
    }

    let info = await Info.find(id)

    Object.assign(info, input)

    const infoId = await info.save()

    if (infoId) {
      return response.json({ status: 'ok', data: infoId })
    }

    return response.json({
      status: 'error',
      message: ERROR_MASSAGE_INFO_IS_EXIST,
    })
  }

  /**
   * Delete a Info with id.
   * DELETE infos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    const { id } = params
    const info = await Info.find(id)

    if (!info) {
      const productIsNotExist = {
        status: 'error',
        message: ERROR_MASSAGE_INFO_IS_NOT_EXIST,
      }
      return response.json(productIsNotExist)
    }

    await info.delete()
    return response.json({ status: 'ok', data: null })
  }
}

module.exports = InfoController
