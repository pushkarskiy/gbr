'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const slugify = require('@sindresorhus/slugify')
const Page = use('App/Models/Page')
const { validate } = use('Validator')

const ERROR_MASSAGE_ADD_PAGE = 'Ошибка добавления страницы'
const ERROR_MASSAGE_PAGE_IS_NOT_EXIST = 'Такая страница не существует'
const ERROR_MASSAGE_PAGE_IS_EXIST = 'Нет изменений для сохранения'

class PageController {
  /**
   * Show a list of all pages.
   * GET pages
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view, params }) {
    const page = request.input('page')
    const limit = request.input('limit')
    const pages = page ? await Page.query().paginate(page, limit) : await Page.all()

    return response.json({ status: 'ok', data: pages })
  }

  /**
   * Create/save a new page.
   * POST temps
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    const rules = {
      title: 'required|min:3|max:100',
      seo_title: 'min:3|max:200',
      seo_description: 'min:3|max:500',
      seo_keywords: 'min:3|max:500',
      content: 'required',
      active: 'boolean',
    }

    const input = request.only(['active', 'title', 'content', 'seo_title', 'seo_description', 'seo_keywords'])
    const validation = await validate(input, rules)

    if (validation.fails()) {
      const validationError = {
        status: 'validationError',
        message: validation.messages()
      }

      return response.json(validationError)
    }

    const data = { ...input, slug: slugify(input.title) }
    const page = await Page.create(data)

    if (!page.id) {
      response.json({ status: 'error', message: ERROR_MASSAGE_ADD_PAGE })
    }

    response.json({ status: 'ok', data: page.id })
  }

  /**
   * Display a single page.
   * GET pages/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
    const { id } = params
    const page = await Page.find(id)

    if (!page) {
      const pageIsNotExist = {
        status: 'error',
        message: ERROR_MASSAGE_PAGE_IS_NOT_EXIST
      }
      return response.json(pageIsNotExist)
    }

    return response.json({ status: 'ok', data: page })
  }

  /**
   * Update page details.
   * PUT or PATCH pages/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const { id } = params
    const input = request.only(['title', 'content', 'active', 'slug', 'seo_title', 'seo_description', 'seo_keywords'])
    const rules = {
      title: 'min:3|max:300',
      content: 'min:3|max:1000', 
      slug: 'min:3|max:1000',
      active: 'boolean',
      seo_title: 'min:3|max:200',
      seo_description: 'min:3|max:500',
      seo_keywords: 'min:3|max:500',
    }

    const validation = await validate(input, rules)

    if (validation.fails()) {
      const validationError = {
        status: 'validationError',
        message: validation.messages()
      }

      return response.json(validationError)
    }

    const page = await Page.find(id)

    Object.assign(page, input)

    const pageId = await page.save()

    if (!pageId) {
      return response.json({
        status: 'error',
        message: ERROR_MASSAGE_PAGE_IS_EXIST
      })
    }

    return response.json({ status: 'ok', data: pageId })
  }

  /**
   * Delete a page with id.
   * DELETE pages/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    const { id } = params
    const page = await Page.find(id)

    if (!page) {
      const pageIsNotExist = {
        status: 'error',
        message: ERROR_MASSAGE_PAGE_IS_NOT_EXIST
      }
      return response.json(pageIsNotExist)
    }

    await page.delete()
    return response.json({ status: 'ok', data: null })
  }
}

module.exports = PageController
