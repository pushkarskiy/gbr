'use strict'

const fs = require('fs')
const path = require('path')
const Helpers = use('Helpers')
const removeFile = Helpers.promisify(fs.unlink)
const File = use('App/Models/File')

const ERROR_MASSAGE_FILE_IS_NOT_EXIST = 'Файл не существует'
const ERROR_MASSAGE_ADD_FILE = 'Ошибка добавления файла'
const EXTS = ['png', 'gif', 'jpeg', 'jpg', 'avi', 'doc', 'docx', 'xls', 'xls', 'zip', 'txt']
const FILE_MAX_SIZE = '5mb'

class FilesApiController {
   /**
   * Show a list of all files.
   * GET files
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    const page = request.input('page')
    const limit = request.input('limit')
    const files = page ? await File.query().paginate(page, limit) : await File.all() 

    return response.json({ status: 'ok', data: files })
  }

  async upload({ request, response }) {
    const file = request.file('file', { size: FILE_MAX_SIZE, extnames: EXTS })
    const outputPath = Helpers.publicPath('uploads')
    const outputFileName = Date.now()
    const outputFile = outputFileName + '.' + file.extname
    const path = '/uploads/' + outputFile
    const data = { value: path }

    await file.move(outputPath, {
      name: outputFile,
      overwrite: true
    })

    const fileData = { client_name: file.clientName, path, size: file.size, extname: file.extname }
    const fileModel = await File.create(fileData)

    if (!fileModel.id) {
      response.json({ status: 'error', message: ERROR_MASSAGE_ADD_FILE })
    }

    if (!file.moved()) {
      const { message } = file.error()
      return response.json({
        status: 'validationError',
        message,
      })
    }

    return response.json({ status: 'ok', data: data })
  }


  /**
   * Delete a file with id.
   * DELETE files/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    const { id } = params
    const file = await File.find(id)

    if (!file) {
      const fileIsNotExist = {
        status: 'error',
        message: ERROR_MASSAGE_FILE_IS_NOT_EXIST
      }
      return response.json(fileIsNotExist)
    }

    await file.delete()
    return response.json({ status: 'ok', data: null })
  }

  /**
   * Delete a file by path.
   * DELETE file
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async delete({ request, response }) {
    const outputPath = Helpers.publicPath()
    const { fileName } = request.get()
    const file = File.findBy('path', fileName)

    if (!file) {
      const fileIsNotExist = {
        status: 'error',
        message: ERROR_MASSAGE_FILE_IS_NOT_EXIST
      }
      return response.json(fileIsNotExist)
    }

    try {
      removeFile(path.join(outputPath, fileName))
      await file.delete()
    } catch (e) {
      return response.json({
        status: 'error',
        message: ERROR_MASSAGE_FILE_IS_NOT_EXIST,
      })
    }

    return response.json({ status: 'ok', data: null })
  }
}

module.exports = FilesApiController
