'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const ERROR_MASSAGE_PAGE_NOT_FOUND = 'Страница не найдена :('
const ERROR_MASSAGE_HANDLER_NOT_FOUND = 'Hendler not found :('

class ErrorController {
	/**
   * Show error page.
   * GET error
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
	async errorPage({ request, response, view }) {
		return view.render('error', { message: ERROR_MASSAGE_PAGE_NOT_FOUND })
	}

	async errorHandler({ request, response, view }) {
		return response.json({ status: 'error', message: ERROR_MASSAGE_HANDLER_NOT_FOUND })
	}
}

module.exports = ErrorController
