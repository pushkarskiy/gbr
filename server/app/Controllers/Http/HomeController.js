'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const MenuService = use('App/Services/MenuService')
const InfoService = use('App/Services/InfoService')
const ProductService = use('App/Services/ProductService')
const ServiceService = use('App/Services/ServiceService')

class HomeController {
  /**
* Show homepage.
* GET infos
*
* @param {object} ctx
* @param {Request} ctx.request
* @param {Response} ctx.response
* @param {View} ctx.view
*/
  async index({ request, response, view, params }) {
    const mainMenu = await MenuService.get('main-menu')
    const aboutCompanyMenu = await MenuService.get('about-company')
    const forClientMenu = await MenuService.get('for-client')
    const infos = await InfoService.get()
    const subMenu = await ServiceService.getSubMenu()
    const servicesPreview = await ServiceService.getServicesPreview()
    const productsPreview = await ProductService.getPreview()
    const data = {
      mainMenu,
      subMenu,
      productsPreview,
      servicesPreview,
      aboutCompanyMenu,
      forClientMenu,
      infos,
    }

    return view.render('pages.home', data)
  }
}

module.exports = HomeController
