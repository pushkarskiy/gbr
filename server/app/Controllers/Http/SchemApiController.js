'use strict'

const infoSchema = require('../../../schems/info')
const productSchema = require('../../../schems/product')
const serviceSchema = require('../../../schems/service')
const menuSchema = require('../../../schems/menu')
const pageSchema = require('../../../schems/page')
const fileSchema = require('../../../schems/file')
const orderSchema = require('../../../schems/order')

class SchemController {
  /**
   * Display a schema for info
   * GET infos/schema
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async schema({ params, request, response }) {
    const schema = this.getSchema(params.name)

    response.json({ status: 'ok', data: schema })
  }

  /**
   * Get schema by name
   * @param {string} name
   */

  getSchema(name) {
    switch (name) {
      case 'order-list':
        return orderSchema.list
      case 'file-list':
        return fileSchema.list
      case 'info-edit-html':
        return infoSchema.editHtml
      case 'info-edit-string':
        return infoSchema.editString
      case 'info-edit-file':
        return infoSchema.editFile
      case 'info-edit-image':
        return infoSchema.editImage
      case 'info-edit-text':
        return infoSchema.editText
      case 'info-create-html':
        return infoSchema.createHtml
      case 'info-create-string':
        return infoSchema.createString
      case 'info-create-file':
        return infoSchema.createFile
      case 'info-create-image':
        return infoSchema.createImage
      case 'info-create-text':
        return infoSchema.createText
      case 'info-list-html':
        return infoSchema.listHtml
      case 'info-list-string':
        return infoSchema.listString
      case 'info-list-file':
        return infoSchema.listFile
      case 'info-list-image':
        return infoSchema.listImage
      case 'info-list-text':
        return infoSchema.listText
      case 'product-list':
        return productSchema.list
      case 'product-edit':
        return productSchema.edit
      case 'product-create':
        return productSchema.create
      case 'service-list':
        return serviceSchema.list
      case 'service-edit':
        return serviceSchema.edit
      case 'service-create':
        return serviceSchema.create
      case 'menu-list':
        return menuSchema.list
      case 'menu-edit':
        return menuSchema.edit
      case 'menu-create':
        return menuSchema.create
      case 'page-list':
        return pageSchema.list
      case 'page-edit':
        return pageSchema.edit
      case 'page-create':
        return pageSchema.create
      default:
        return []
    }
  }
}

module.exports = SchemController
