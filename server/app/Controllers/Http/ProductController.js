'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const MenuService = use('App/Services/MenuService')
const InfoService = use('App/Services/InfoService')
const ProductService = use('App/Services/ProductService')
const ServiceService = use('App/Services/ServiceService')

class ProductController {
  /**
  * Show all services.
  * GET services
  *
  * @param {object} ctx
  * @param {Request} ctx.request
  * @param {Response} ctx.response
  * @param {View} ctx.view
  */
  async index({ request, response, view, params }) {

    const mainMenu = await MenuService.get('main-menu')
    const aboutCompanyMenu = await MenuService.get('about-company')
    const forClientMenu = await MenuService.get('for-client')
    const infos = await InfoService.get()
    const subMenu = await ServiceService.getSubMenu()
    const products = await ProductService.getAll()
    const data = {
      mainMenu,
      aboutCompanyMenu,
      forClientMenu,
      infos,
      products,
      subMenu
    }

    return view.render('pages.products', data)
  }

  /**
  * Show detail page.
  * GET service
  *
  * @param {object} ctx
  * @param {Request} ctx.request
  * @param {Response} ctx.response
  * @param {View} ctx.view
  */
  async show({ request, response, view, params }) {
    const exist = await ProductService.check(params.slug)

    if (!exist) response.redirect('/404', false, 307)

    const mainMenu = await MenuService.get('main-menu')
    const aboutCompanyMenu = await MenuService.get('about-company')
    const forClientMenu = await MenuService.get('for-client')
    const infos = await InfoService.get()
    const subMenu = await ServiceService.getSubMenu()
    const productsPreview = await ProductService.getPreview()
    const breadcrumbs = await ProductService.getBreadcrumbLastItem(params.slug)
    const content = await ProductService.getContent(params.slug)
    const data = {
      mainMenu,
      aboutCompanyMenu,
      forClientMenu,
      infos,
      breadcrumbs,
      content,
      subMenu,
      productsPreview
    }

    return view.render('pages.product', data)
  }
}

module.exports = ProductController
