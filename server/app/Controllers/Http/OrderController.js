'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Order = use('App/Models/Order')
const { validate } = use('Validator')

const ERROR_MASSAGE_ADD_ORDER = 'Не удалось создать заказ'

class OrderController {
  /**
   * Show a list of all orders.
   * GET orders
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const input = request.only(['name', 'phone', 'email', 'order_type', 'type_id']); 
    const rules = {
      name: 'required',
      phone: 'required|number',
      email: 'email',
      order_type: 'string',
      type_id: 'number'
    }

    const validation = await validate(input, rules)

    if (validation.fails()) {
      const validationError = {
        status: 'validationError',
        message: validation.messages(),
      }

      return response.json(validationError)
    }

    const order = await Order.create(input)
    
    if (!order.id) {
      response.json({ status: 'error', message: ERROR_MASSAGE_ADD_ORDER })
    }

    return response.json({ status: 'ok', data: null })
  }
}

module.exports = OrderController
