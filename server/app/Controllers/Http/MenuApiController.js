'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Menu = use('App/Models/Menu')
const { validate } = use('Validator')

const ERROR_MASSAGE_ADD_MENU = 'Ошибка добавления пункта меню'
const ERROR_MASSAGE_MENU_IS_EXIST = 'Нет изменений для сохранения'
const ERROR_MASSAGE_MENU_IS_NOT_EXIST = 'Такой пункт меню не существует'

class MenuController {
  /**
   * Show a list of all menus.
   * GET menus
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    const menus = await Menu.query().orderBy('category', 'desc').fetch()

    return response.json({ status: 'ok', data: menus })
  }

  /**
   * Create/save a new menu.
   * POST temps
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    const rules = {
      title: 'required|min:3|max:100',
      link: 'required|min:3',
      category: 'required|min:3|max:100',
      active: 'boolean',
    }

    const input = request.only(['active', 'title', 'link', 'category'])
    const validation = await validate(input, rules)

    if (validation.fails()) {
      const validationError = {
        status: 'validationError',
        message: validation.messages(),
      }

      return response.json(validationError)
    }

    const menu = await Menu.create(input)

    if (!menu.id) {
      response.json({ status: 'error', message: ERROR_MASSAGE_ADD_MENU })
    }

    response.json({ status: 'ok', data: menu.id })
  }

  /**
   * Display a single menu.
   * GET menus/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
    const { id } = params
    const menu = await Menu.find(id)

    if (!menu) {
      const menuIsNotExist = {
        status: 'error',
        message: ERROR_MASSAGE_MENU_IS_NOT_EXIST,
      }
      return response.json(menuIsNotExist)
    }

    return response.json({ status: 'ok', data: menu })
  }

  /**
   * Update menu details.
   * PUT or PATCH menus/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const { id } = params
    const input = request.only(['title', 'category', 'link', 'active', 'position'])
    const rules = {
      title: 'min:3|max:100',
      category: 'min:3',
      link: 'min:3',
      active: 'boolean',
    }

    const validation = await validate(input, rules)

    if (validation.fails()) {
      const validationError = {
        status: 'validationError',
        message: validation.messages(),
      }

      return response.json(validationError)
    }

    const menu = await Menu.find(id)

    Object.assign(menu, input)

    const menuId = await menu.save()

    if (!menuId) {
      return response.json({
        status: 'error',
        message: ERROR_MASSAGE_MENU_IS_EXIST,
      })
    }

    return response.json({ status: 'ok', data: menuId })
  }

  /**
   * Delete a menu with id.
   * DELETE menus/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    const { id } = params
    const menu = await Menu.find(id)

    if (!menu) {
      const menuIsNotExist = {
        status: 'error',
        message: ERROR_MASSAGE_MENU_IS_NOT_EXIST,
      }
      return response.json(menuIsNotExist)
    }

    await menu.delete()
    return response.json({ status: 'ok', data: null })
  }
}

module.exports = MenuController
