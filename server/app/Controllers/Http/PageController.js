'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const MenuService = use('App/Services/MenuService')
const InfoService = use('App/Services/InfoService')
const ProductService = use('App/Services/ProductService')
const ServiceService = use('App/Services/ServiceService')
const PageService = use('App/Services/PageService')

/**
 * Resourceful controller for interacting with pages
 */
class PageController {
  /**
   * Show a list of all pages.
   * GET pages
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view, params }) {
    const exist = await PageService.check(params.slug)

    if (!exist) response.redirect('/404', false, 307)

    const mainMenu = await MenuService.get('main-menu')
    const aboutCompanyMenu = await MenuService.get('about-company')
    const forClientMenu = await MenuService.get('for-client')
    const infos = await InfoService.get()
    const subMenu = await ServiceService.getSubMenu()
    const breadcrumbs = await PageService.getBreadcrumbLastItem(params.slug)
    const servicesList = await ServiceService.getServicesPreview()
    const productsPreview = await ProductService.getPreview()
    const content = await PageService.getPageBySlug(params.slug)
    const data = {
      mainMenu,
      subMenu,
      productsPreview,
      servicesList,
      aboutCompanyMenu,
      forClientMenu,
      infos,
      content,
      breadcrumbs
    }

    return view.render('pages.page', data)
  }

  /**
   * Render a form to be used for creating a new page.
   * GET pages/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {}

  /**
   * Create/save a new page.
   * POST pages
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {}

  /**
   * Display a single page.
   * GET pages/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {}

  /**
   * Render a form to update an existing page.
   * GET pages/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update page details.
   * PUT or PATCH pages/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {}

  /**
   * Delete a page with id.
   * DELETE pages/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {}
}

module.exports = PageController
