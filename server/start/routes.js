'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.group('rest', () => {
  Route.get('/products', 'ProductApiController.index')
  Route.post('/products/create', 'ProductApiController.store')
  Route.get('/products/:id', 'ProductApiController.show')
  Route.put('/products/:id', 'ProductApiController.update')
  Route.delete('/products/:id', 'ProductApiController.destroy')

  Route.get('/pages', 'PageApiController.index')
  Route.post('/pages/create', 'PageApiController.store')
  Route.get('/pages/:id', 'PageApiController.show')
  Route.put('/pages/:id', 'PageApiController.update')
  Route.delete('/pages/:id', 'PageApiController.destroy')

  Route.get('/services', 'ServiceApiController.index')
  Route.post('/services/create', 'ServiceApiController.store')
  Route.get('/services/:id', 'ServiceApiController.show')
  Route.put('/services/:id', 'ServiceApiController.update')
  Route.delete('/services/:id', 'ServiceApiController.destroy')

  Route.get('/menus', 'MenuApiController.index')
  Route.post('/menus/create', 'MenuApiController.store')
  Route.get('/menus/:id', 'MenuApiController.show')
  Route.put('/menus/:id', 'MenuApiController.update')
  Route.delete('/menus/:id', 'MenuApiController.destroy')

  Route.get('/infos/:type', 'InfoApiController.index')
  Route.post('/infos/:type/create', 'InfoApiController.store')
  Route.get('/infos/:type/:id', 'InfoApiController.show')
  Route.put('/infos/:type/:id', 'InfoApiController.update')
  Route.delete('/infos/:type/:id', 'InfoApiController.destroy')

  Route.get('/schema/:name', 'SchemApiController.schema')
  Route.get('/files', 'FilesApiController.index')
  Route.post('/files/upload', 'FilesApiController.upload')
  Route.delete('/files/delete', 'FilesApiController.delete')
  Route.delete('/files/:id', 'FilesApiController.destroy')

  Route.get('/orders', 'OrderApiController.index')
  Route.delete('/orders/:id', 'OrderApiController.destroy')
}).prefix('api/v1')

Route.get('/', 'HomeController.index')
Route.get('/services', 'ServiceController.index')
Route.get('/services/:slug', 'ServiceController.show')

Route.get('/products', 'ProductController.index')
Route.get('/products/:slug', 'ProductController.show')

Route.get('/pages/:slug', 'PageController.index')

Route.get('/contacts', 'ContactController.index')

Route.post('/order', 'OrderController.index')
Route.get('/404', 'ErrorController.errorPage')
Route.post('*', 'ErrorController.errorHandler')
Route.any('*', 'ErrorController.errorPage')
