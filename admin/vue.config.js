const path = require('path')
const MonacoWebpackPlugin = require('monaco-editor-webpack-plugin')

module.exports = {
  devServer: {
    proxy: {
      '/api/v1': {
        target: 'http://localhost:3333'
      }
    }
  },
  configureWebpack: {
    plugins: [
      new MonacoWebpackPlugin()
    ]
  },
  chainWebpack: (config) => {
    config.resolve.alias.set('~', path.resolve(__dirname, './'))
  }
}
