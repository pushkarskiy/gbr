export function formatFileInfo({ name, size, lastModified, type }) {
  const date = new Date(lastModified)
  const options = {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
    second: 'numeric'
  }

  return {
    name,
    type,
    size: fileSize(size),
    lastModified: date.toLocaleString('ru', options)
  }
}

export function fileSize(length) {
  const type = ['б', 'Кб', 'Мб', 'Гб']
  let i = 0
  while ((length / 1000) | 0 && i < type.length - 1) {
    length /= 1024
    i++
  }
  return length.toFixed(2) + ' ' + type[i]
}
