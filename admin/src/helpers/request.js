import axios from 'axios'
import store from '@/store'

const request = axios.create({
  baseURL: store.state.baseUrl,
  timeout: 5000
})

export default request
