import Vue from 'vue'
import Router from 'vue-router'
import Products from './views/Products/List'
import ProductEdit from './views/Products/Edit'
import ProductCreate from './views/Products/Create'
import Pages from './views/Pages/List'
import PageEdit from './views/Pages/Edit'
import PageCreate from './views/Pages/Create'
import Services from './views/Services/List'
import ServiceEdit from './views/Services/Edit'
import ServiceCreate from './views/Services/Create'
import Menus from './views/Menus/List'
import MenuEdit from './views/Menus/Edit'
import MenuCreate from './views/Menus/Create'
import Infos from './views/Infos/List'
import InfoEdit from './views/Infos/Edit'
import InfoCreate from './views/Infos/Create'
import Files from './views/Files/List'
import Orders from './views/Orders/List'
import Home from './views/Home'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/products',
      name: 'products',
      component: Products
    },
    {
      path: '/products/create',
      name: 'product-create',
      component: ProductCreate
    },
    {
      path: '/products/edit/:id',
      name: 'product-edit',
      component: ProductEdit
    },
    {
      path: '/pages',
      name: 'pages',
      component: Pages
    },
    {
      path: '/pages/create',
      name: 'page-create',
      component: PageCreate
    },
    {
      path: '/pages/edit/:id',
      name: 'page-edit',
      component: PageEdit
    },
    {
      path: '/services',
      name: 'services',
      component: Services
    },
    {
      path: '/services/create',
      name: 'service-create',
      component: ServiceCreate
    },
    {
      path: '/services/edit/:id',
      name: 'service-edit',
      component: ServiceEdit
    },
    {
      path: '/menus',
      name: 'menus',
      component: Menus
    },
    {
      path: '/menus/create',
      name: 'menu-create',
      component: MenuCreate
    },
    {
      path: '/menus/edit/:id',
      name: 'menu-edit',
      component: MenuEdit
    },
    {
      path: '/infos',
      name: 'infos',
      component: Infos
    },
    {
      path: '/infos/:type/create',
      name: 'info-create',
      component: InfoCreate
    },
    {
      path: '/infos/:type/edit/:id',
      name: 'info-edit',
      component: InfoEdit
    },
    {
      path: '/files',
      name: 'file-list',
      component: Files
    },
    {
      path: '/orders',
      name: 'order-list',
      component: Orders
    }
  ]
})
