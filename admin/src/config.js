const hostname = document.location.hostname;

const config = {
  development: {
    serverHost: `http://${hostname}:3333`
  },
  production: {
    serverHost: `http://${hostname}`
  }
}

export default config[process.env.NODE_ENV]
