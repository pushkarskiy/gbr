import request from '@/helpers/request.js'
import store from './store'

const schemaEndPoint = (schemaName) => `/schema/${schemaName}`
const showEndPoint = (endPoint, id) => `${endPoint}/${id}`
const saveEndPoint = (endPoint, id) => `${endPoint}/${id}`
const createEndPoint = (endPoint) => `${endPoint}/create`
const listEndPoint = (endPoint) => `${endPoint}`
const deleteEndPoint = (endPoint, id) => `${endPoint}/${id}`
const fileUploadEndPoint = (endPoint) => `${endPoint}/`
const fileDeleteEndPoint = (endPoint, file) => `${endPoint}/?fileName=${file}`

// Схема по которой будет отрисована форма для создания и редактирования
export function getSchema(schemaName) {
  return request.get(schemaEndPoint(schemaName)).then(checkResponse).catch(console.log)
}

// Получение одного элемента
export function get(endPoint, id) {
  return request.get(showEndPoint(endPoint, id)).then(checkResponse).catch(console.log)
}

// Получение списка элементов
export function getList(endPoint) {
  return request.get(listEndPoint(endPoint)).then(checkResponse).catch(console.log)
}

// Сохраняем элемента
export function del(endPoint, id) {
  return request.delete(deleteEndPoint(endPoint, id)).then(checkResponse).catch(console.log)
}

// Удалением элемента
export function save(endPoint, id, payload) {
  return request.put(saveEndPoint(endPoint, id), payload).then(checkResponse).catch(console.log)
}

// Создаем элемента
export function create(endPoint, payload) {
  console.log(payload)
  return request.post(createEndPoint(endPoint), payload).then(checkResponse).catch(console.log)
}

// Загрузка файлов
export function fileUpload(endPoint, formData, options) {
  return request.post(fileUploadEndPoint(endPoint), formData, options).then(checkResponse).catch(console.log)
}

// Удаление файлов
export function fileDelete(endPoint, file) {
  return request.delete(fileDeleteEndPoint(endPoint, file)).then(checkResponse).catch(console.log)
}

// Проверка ответа от сервера на валидность
function checkResponse(response) {
  const { status = null, data = null, message } = response.data

  if (response.status >= 200 && response.status < 300) {
    if (status === 'validationError') {
      store.dispatch('ADD_VALIDATION_MESSAGE', message)
      return
    }
    if (status === 'error') {
      store.dispatch('ADD_ERROR_MESSAGE', message)
      return
    }

    store.dispatch('CLEAR_ERROR_MESSAGE')
    store.dispatch('CLEAR_VALIDATION_MESSAGE')

    return Promise.resolve(data)
  }
  throw new Error('Неизвестная ошибка', response)
}
