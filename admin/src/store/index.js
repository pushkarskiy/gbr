import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: true,
  state: {
    baseUrl: '/api/v1',
    validationMessages: [],
    errorMessages: '',
    successMessage: ''
  },
  modules: {},
  mutations: {
    addValidationMessages(state, payload) {
      Vue.set(state, 'validationMessages', payload)
    },
    clearValidationMessages(state) {
      Vue.set(state, 'validationMessages', [])
    },
    addErrorMessages(state, payload) {
      Vue.set(state, 'errorMessage', payload)
    },
    clearErrorMessages(state) {
      Vue.set(state, 'errorMessage', '')
    },
    addSuccessMessages(state, payload) {
      Vue.set(state, 'successMessage', payload)
    },
    clearSuccessMessages(state) {
      Vue.set(state, 'successMessage', '')
    }
  },
  actions: {
    ADD_VALIDATION_MESSAGE(context, payload) {
      context.commit('addValidationMessages', payload)
    },
    CLEAR_VALIDATION_MESSAGE(context) {
      context.commit('clearValidationMessages')
    },
    ADD_ERROR_MESSAGE(context, payload) {
      context.commit('addErrorMessages', payload)
    },
    CLEAR_ERROR_MESSAGE(context) {
      context.commit('clearErrorMessages')
    },
    ADD_SUCCESS_MESSAGE(context, payload) {
      context.commit('addSuccessMessages', payload)

      setTimeout(() => {
        context.dispatch('CLEAR_SUCCESS_MESSAGE')
      }, 3000)
    },
    CLEAR_SUCCESS_MESSAGE(context) {
      context.commit('clearSuccessMessages')
    }
  }
})
