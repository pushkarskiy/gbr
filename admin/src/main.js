import 'vuetify/dist/vuetify.min.css'

import { sync } from 'vuex-router-sync'
import Vuetify from 'vuetify'
import CKEditor from '@ckeditor/ckeditor5-vue'

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/index'

Vue.config.productionTip = false

Vue.use(Vuetify)
Vue.use(CKEditor)
sync(store, router)

new Vue({
  strict: true,
  router,
  store,
  render: (h) => h(App)
}).$mount('#app')
