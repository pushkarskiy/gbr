# Настройка OS Ubuntu 18.10

## Установка GEI
- `sudo apt-get install git`

## Установка NVM
- `sudo apt purge nodejs`
- `sudo apt install build-essential checkinstall`
- `sudo apt install libssl-dev`
- `wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash`
- `source ~/.bashrc`

## Копирование SRC
- `cd /var/www/html/`
- `mkdir gbr`
- `cd gbr`
- `git clone https://pushkarskiy@bitbucket.org/pushkarskiy/gbr.git`
- `cd admin`
- `yarn install`
- `yarn run build`
- `cd ../server`
- `yarn install`
- `yarn run build`

## Установка NGINX
- `sudo apt update`
- `sudo apt install nginx`
- `sudo systemctl enable nginx`
- `rm /etc/nginx/nginx.conf`
- `cd /var/www/html/gbr/`
- `cp nginx.conf /etc/nginx/`
- `cp .htpasswd /usr/share/nginx/`
- `nginx -s reload`

## Настройка автозапуска после рестарта сервера (SYSTEMD)
- `sudo cp node-gbr.service /etc/systemd/system/node-gbr.service`
- `sudo systemctl daemon-reload`
- `sudo systemctl node-gbr.service`
- `sudo systemctl start node-gbr.service`
- `sudo systemctl enable node-gbr.service`
